package com.example.permissionsapp;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;



public class ApplicationData {
	private PlatformVersion lastPlatformVersion;
	private Verifier verifier;
	private List<Permission> permissionTree;
	private List<Permission> permissions;
	protected List<Package> apps1;
	private Package updatedPackage;
	private String preferredActivites;
	protected List<SharedUser> apps;
	
	public ApplicationData(){
		apps = new ArrayList<SharedUser>();
		//Log.d("ERROR", "new app data");
	}
	
	public ApplicationData(List<SharedUser> _apps){
		apps = _apps;
	}
	
	public List<SharedUser> getSharedUsers(){
		return apps;
	}
	
	public List<Package> getPackages(){
		return apps1;
	}
	

}
