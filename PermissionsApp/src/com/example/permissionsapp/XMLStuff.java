package com.example.permissionsapp;

import java.io.File;
import java.io.FileWriter;

import android.util.Log;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.CommandCapture;
import com.stericson.RootTools.execution.Shell;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class XMLStuff{
	protected static ApplicationData appData;
	private static XStream xstream;

	public void init(){
		xstream = new XStream();
			 /* @Override
			  protected MapperWrapper wrapMapper(MapperWrapper next) {
			    return new MapperWrapper(next) {
			      @Override
			      public boolean shouldSerializeMember(Class definedIn,
			              String fieldName) {
			        if (definedIn == Object.class) {
			          return false;
			        }
			        return super.shouldSerializeMember(definedIn, fieldName);
			      }
			    };
			  }
			}*/
		xstream.alias("packages", ApplicationData.class);
		xstream.aliasField("last-platform-version", ApplicationData.class, "lastPlatformVersion");
		xstream.alias("last-platform-version", PlatformVersion.class);
		xstream.alias("verifier", Verifier.class);
		xstream.useAttributeFor(PlatformVersion.class, "internal");
		xstream.useAttributeFor(PlatformVersion.class, "external");
		xstream.useAttributeFor(Verifier.class, "device");
		xstream.aliasField("permission-trees", ApplicationData.class, "permissionTree");
		xstream.addImplicitCollection(ApplicationData.class, "apps1");
		xstream.alias("package", Package.class);
		xstream.addImplicitCollection(ApplicationData.class, "apps");
		xstream.aliasField("disabled-components", Package.class, "disabledComponents");
		xstream.aliasField("enabled-components", Package.class, "enabledComponents");
		xstream.alias("item", Item.class);
		xstream.useAttributeFor(Item.class, "name");
		xstream.useAttributeFor(Item.class, "p");
		xstream.useAttributeFor(Package.class, "codePath");
		xstream.useAttributeFor(Package.class, "nativeLibraryPath");
		xstream.useAttributeFor(Package.class, "flags");
		xstream.useAttributeFor(Package.class, "ft");
		xstream.useAttributeFor(Package.class, "it");
		xstream.useAttributeFor(Package.class, "ut");
		xstream.useAttributeFor(Package.class, "version");
		xstream.useAttributeFor(Package.class, "sharedUserId");
		xstream.aliasField("updated-package", ApplicationData.class, "updatedPackage");
		xstream.aliasField("preferred-activities", ApplicationData.class, "preferredActivites");
		xstream.alias("shared-user", SharedUser.class);
		xstream.alias("sigs", Sigs.class);
		xstream.alias("cert", Cert.class);
		xstream.addImplicitCollection(Sigs.class, "certs");
		xstream.alias("item", Permission.class);
		xstream.useAttributeFor(SharedUser.class, "name");
		xstream.useAttributeFor(SharedUser.class, "userId");
		xstream.useAttributeFor(Sigs.class, "count");
		xstream.useAttributeFor(Cert.class, "index");
		xstream.useAttributeFor(Cert.class, "key");
		xstream.useAttributeFor(Permission.class, "name");
		xstream.useAttributeFor(Permission.class, "protection");
		xstream.useAttributeFor(Permission.class, "pack");
		xstream.aliasField("package", Permission.class, "pack");
	}

	public void getAppInfo(){
		if (RootTools.isRootAvailable()){
			File file = new File("/data/system/packages.xml");
			appData = (ApplicationData)xstream.fromXML(file);
			Log.i("ERROR", "got app info");
			if (appData.getSharedUsers()==null){
				Log.e("ERROR", "shared users is null");
			}
		}
	}

	private static void changeAppInfo(){
		RootTools.remount("/data", "RW");
		Shell myShell=null;
		try{
			myShell = RootTools.getShell(true);
			RootTools.runShellCommand(myShell, new CommandCapture(0, "su"));
			RootTools.runShellCommand(myShell, new CommandCapture(0, "chmod -R 777 /data/local"));
		}catch(Exception e){
			Log.i("ERROR", "first shell error");
		}
		File newFile = new File("/data/local/packages1.xml");
		Log.d("ERROR", newFile.getAbsolutePath());
		try{
			FileWriter fw = new FileWriter(newFile);
			Log.i("ERROR", "Check1");
			xstream.toXML(appData, fw);
			Log.i("ERROR", "Check2");
			fw.close();
		}catch (Exception e){
			Log.e("ERROR", "another file writing error");
			Log.e("ERROR", e.getMessage().toString());
			Log.e("ERROR", e.toString());
		}

		try{
			RootTools.runShellCommand(myShell, new CommandCapture(0, "chmod 777 /data/system/packages.xml"));
			RootTools.runShellCommand(myShell, new CommandCapture(0, "chmod 777 /tmp/packages2935.xml"));
			RootTools.runShellCommand(myShell, new CommandCapture(0, "dd if=/data/local/packages1.xml of=/data/system/packages.xml"));
			RootTools.runShellCommand(myShell, new CommandCapture(0, "dd if=/data/system/packages.xml of=/tmp/packages2935.xml"));
			RootTools.runShellCommand(myShell, new CommandCapture(0, "rm /data/local/packages1.xml"));
			RootTools.closeAllShells();
		}catch (Exception e){
			Log.e("ERROR", "file did not copy");
		}

	}
	
	protected static void activatePermission(int appIndex, int permIndex){
		Permission perm = appData.apps.get(appIndex).perms.get(permIndex);
		perm.activate();
		changeAppInfo();
		
	}
	
	protected static void denyPermission(int appIndex, int permIndex){
		Permission perm = appData.apps.get(appIndex).perms.get(permIndex);
		perm.deny();
		Log.d("ERROR", perm.getName());
		changeAppInfo();
		Log.d("ERROR", perm.getName());
	}
	public String getXML(){
		return xstream.toXML(appData);
	}
}


