package com.example.permissionsapp;

import android.util.Log;

public class Permission {
	private String name;
	private String pack;
	private String protection;

	public Permission(){

	}

	public Permission(String _name){
		name = _name;
	}

	public String getName(){
		return name;
	}

	protected void activate(){
		//Log.i("ERROR", name + ": " + name.contains("Denied2935"));
		name=name.replaceAll("Denied2935", "");
		//Log.d("ERROR", name + ": " + name.contains("Denied2935"));
	}

	protected void deny(){
		name = "Denied2935" + name;
	}
}
