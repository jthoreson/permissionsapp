package com.example.permissionsapp;

import java.util.List;

import android.util.Log;

public class Package extends SharedUser{
	private String codePath;
	private String nativeLibraryPath;
	private String flags;
	private String ft;
	private String it;
	private String ut;
	private String version;
	private String sharedUserId;
	private List<Item> disabledComponents;
	private List<Item> enabledComponents;
	
	public Package(){
		//Log.i("ERROR", "new package");
	}
	
	public Package(String _name, List<Permission> _perms){
		name = _name;
		perms = _perms;
	}
	
	public String getName(){
		return name;
	}
}