package com.example.permissionsapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;

public class DenyPermissions extends Activity {
	public static final String INDEX = "this is another tag string";
	public static final String APP_INDEX = "app index tag";
	private CheckBox cb;
	private int appIndex = -1;
	private int permIndex = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_deny_permissions);
		Intent intent = getIntent();
		String permName="";
		try{
			appIndex = intent.getIntExtra(APP_INDEX, -1);
		}catch(Exception e){
			appIndex = -1;
		}
		try{
			permIndex = intent.getIntExtra(INDEX, -1);
		}catch(Exception e){
			permName = "This app has no permissions";
		}
		if (appIndex>=0&&permIndex>=0){
			Log.d("ERROR", "doing stuff");
			permName = XMLStuff.appData.apps.get(appIndex).perms.get(permIndex).getName();
			Log.d("ERROR", permName);
		}
		cb = (CheckBox)findViewById(R.id.checkBox1);
		cb.setText("Permission active?");
		cb.setChecked(!permName.contains("Denied2935"));
		Button button = (Button)findViewById(R.id.button1);
		button.setText("Save Changes");
		Button buttonCancel = (Button)findViewById(R.id.button2);
		buttonCancel.setText("Cancel");
		final Context context = this;
		button.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				if (permIndex>=0&&appIndex>=0){
					if (cb.isChecked()){
						XMLStuff.activatePermission(appIndex, permIndex);
					}else{
						XMLStuff.denyPermission(appIndex, permIndex);
						/*XMLStuff xmls = new XMLStuff();
						xmls.init();
						xmls.getAppInfo();
						Log.v("ERROR", XMLStuff.appData.apps.get(appIndex).perms.get(permIndex).getName());*/
					}
					Intent intent = new Intent(context, AppsListActivity.class);
					startActivity(intent);
				}
				

			}
		});
		buttonCancel.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				Intent intent = new Intent(context, AppsListActivity.class);
				startActivity(intent);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_deny_permissions, menu);
		return true;
	}
}
