package com.example.permissionsapp;

import android.os.Bundle;
import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.widget.TextView;

public class PackagesDisplay extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages_display);
        TextView tv = (TextView)findViewById(R.id.textView1);
        tv.setMovementMethod(new ScrollingMovementMethod());
        XMLStuff handler = new XMLStuff();
        handler.init();
        handler.getAppInfo();
        tv.setText(handler.getXML());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_packages_display, menu);
        return true;
    }
}
