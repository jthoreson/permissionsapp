package com.example.permissionsapp;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PermissionActivity extends ListActivity {
	public static final String APP_INDEX="this is a string";
	private ArrayAdapter<String> aa;
	private SharedUser sharedU = new SharedUser(null, null);
	private int index=-1;
	private ArrayList<String> permStrings;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_permission);
		Intent intent = getIntent();
		List<Permission> permissions = new ArrayList<Permission>();
		try{
			index = intent.getIntExtra(PermissionActivity.APP_INDEX, -1);
		}catch(Exception e){
			Log.i("ERROR", "no int");
		}
		if (index>=0&&index<XMLStuff.appData.getSharedUsers().size()){
			Log.i("ERROR", "getting permissions");
			sharedU = XMLStuff.appData.getSharedUsers().get(index);
		}
		try {
			permissions = sharedU.getPerms();
		}catch(Exception e){
			permissions = new ArrayList<Permission>();
		}
		if (permissions!=null&&permissions.size()>0){
			Log.i("ERROR","first perm " + permissions.get(0).getName());
		}
		permStrings = new ArrayList<String>();
		Log.i("ERROR", "check 1");
		if (permissions!=null&&permissions.size()>0){
			for (Permission p: permissions){
				permStrings.add(parsePermName(p.getName()));
			}
		}else{
			permStrings.add("No Permissions");
		}
		permStrings.add("Back to apps list");
		int layoutID = android.R.layout.simple_list_item_1;
		aa = new ArrayAdapter<String>(this, layoutID, permStrings);
		setListAdapter(aa);
	}

	protected void onListItemClick(ListView l, View v, int position, long id){
		if(permStrings.get(position).equals("Back to apps list")){
			Intent intent = new Intent(this, AppsListActivity.class);
			startActivity(intent);
		}else if (!permStrings.get(0).equals("No Permissions")){
			Intent intent = new Intent(this, DenyPermissions.class);
			intent.putExtra(DenyPermissions.INDEX, position);
			intent.putExtra(DenyPermissions.APP_INDEX, index);
			startActivity(intent);
		}
	}

	private String parsePermName(String perm){
		String nString = new String();
		int index = perm.indexOf("permission.")+11;
		perm=perm.substring(index);
		do{
			nString = nString+perm.substring(0,1);
			index = perm.indexOf("_");
			if (index<0){
				nString = nString+perm.substring(1).toLowerCase();
			}else{
				nString = nString+perm.substring(1,index).toLowerCase() + " ";
				perm = perm.substring(index+1);
			}
		}while (index>0);
		return nString;
	}
}
