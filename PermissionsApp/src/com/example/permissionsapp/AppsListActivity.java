package com.example.permissionsapp;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;
import com.stericson.RootTools.execution.CommandCapture;
import com.stericson.RootTools.execution.Shell;

public class AppsListActivity extends ListActivity {
	private ArrayAdapter<String> aa;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps);
		try{
			Shell shell = RootTools.getShell(true);
			RootTools.runShellCommand(shell, new CommandCapture(0, "chmod 777 /data"));
			RootTools.runShellCommand(shell, new CommandCapture(0, "dd if=/tmp/packages2935.xml of=/data/system/packages.xml"));
			RootTools.closeAllShells();
		}catch(Exception e){
			Toast.makeText(this, "Unfortunately, you do not have root, and this app will not perform its intended function.", Toast.LENGTH_SHORT).show();
		}
		XMLStuff handler = new XMLStuff();
		handler.init();
		handler.getAppInfo();
		ArrayList<String> apps = new ArrayList<String>();
		for (SharedUser s: XMLStuff.appData.getSharedUsers()){
			apps.add(s.getName());
		}
		final PackageManager pm = getPackageManager();
		ArrayList<String> appNames = new ArrayList<String>();
		for (String s:apps){
			try{
				ApplicationInfo ai = pm.getApplicationInfo(s, 0);
				appNames.add((String)pm.getApplicationLabel(ai));
			}catch(Exception e){
				appNames.add(s);
			}
			int layoutID = android.R.layout.simple_list_item_1;
			aa=new ArrayAdapter<String>(this, layoutID, appNames);
			setListAdapter(aa);
		}

	}
	
	protected void onListItemClick(ListView l, View v, int position, long id){
		Intent intent = new Intent(this, PermissionActivity.class);
		intent.putExtra(PermissionActivity.APP_INDEX, position);
		startActivity(intent);
	}
	/*public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(0, 1, Menu.NONE, "Reboot");
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item){
		super.onOptionsItemSelected(item);
		switch(item.getItemId()){
		case (1):{
			Shell myShell = RootTools.getShell(false);
			
			return true;
		}
		}
		return false;
	}*/
}
