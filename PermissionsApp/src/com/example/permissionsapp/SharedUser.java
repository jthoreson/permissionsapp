package com.example.permissionsapp;

import java.util.List;

import android.util.Log;


public class SharedUser {
	protected String name;
	protected String userId;
	protected Sigs sigs;
	protected List<Permission> perms;
	
	public SharedUser(){
		//Log.d("ERROR", "new shared user");
	}
	
	public SharedUser(String _name, List<Permission> _perms){
		name = _name;
		perms = _perms;
	}
	
	public String getName(){
		return name;
	}
	
	public List<Permission> getPerms(){
		return perms;
	}
}
